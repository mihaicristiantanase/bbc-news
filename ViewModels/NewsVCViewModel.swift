// ©2019 Mihai Cristian Tanase. All rights reserved.

import UIKit

enum State {
  case loading
  case error
  case empty
  case done
}

protocol NewsVCViewModelDelegate: class {
  func newsVCViewModelOpenUrl(_: URL)
}

class NewsVCViewModel {
  weak var delegate: NewsVCViewModelDelegate?

  private var service: BBCNewsService!
  var items: [NewsModel]?
  private var state: State?
  var currentState: State? {
    return state
  }

  func load(_ callback: @escaping () -> Void) {
    if state == .loading { return }

    if service == nil {
      service = BBCNewsService()
    }
    state = .loading
    callback()
    service.load { [weak self] items in
      self?.items = items
      self?.state = items == nil ? .error : items!.isEmpty ? .empty : .done
      callback()
    }
  }

  func open(_ itemIdx: Int) {
    let item = items![itemIdx]
    guard let url = item.link else {
      return
    }
    if AppSettings.openNewsInApp {
      delegate?.newsVCViewModelOpenUrl(url)
    } else {
      UIApplication.shared.open(url)
    }
  }
}
