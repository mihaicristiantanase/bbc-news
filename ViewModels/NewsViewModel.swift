// ©2019 Mihai Cristian Tanase. All rights reserved.

import UIKit

struct NewsViewModel {
  let item: NewsModel?

  var title: String {
    return item?.title ?? ""
  }

  var description: String {
    return item?.description ?? ""
  }

  var date: String {
    return stringFromDate(item?.date)
  }

  var picUrl: URL? {
    return item?.pic
  }
}

private var df: DateFormatter {
  let formatter = DateFormatter()
  formatter.dateFormat = "dd/mm/yyyy"
  return formatter
}

private func stringFromDate(_ date: Date?) -> String {
  guard let date = date else {
    return ""
  }
  return df.string(from: date)
}
