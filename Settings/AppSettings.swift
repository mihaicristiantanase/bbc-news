// ©2019 Mihai Cristian Tanase. All rights reserved.

import UIKit

private let ud = UserDefaults.standard
private let openNewsInAppKey = "openNewsInAppKey"

class AppSettings {
  static var openNewsInApp: Bool {
    get {
      return ud.bool(forKey: openNewsInAppKey)
    }
    set {
      ud.set(newValue, forKey: openNewsInAppKey)
      ud.synchronize()
    }
  }
}
