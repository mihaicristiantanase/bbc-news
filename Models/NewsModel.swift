// ©2019 Mihai Cristian Tanase. All rights reserved.

import UIKit

struct NewsModel {
  let title: String
  let description: String
  let date: Date?
  let link: URL?
  let pic: URL?
}
