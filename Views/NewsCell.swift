// ©2019 Mihai Cristian Tanase. All rights reserved.

import UIKit

class NewsCell: UICollectionViewCell {
  @IBOutlet var titleL: UILabel!
  @IBOutlet var descriptionL: UILabel!
  @IBOutlet var dateL: UILabel!
  @IBOutlet var picIV: UIImageView!

  var newsVM: NewsViewModel? {
    didSet {
      updateUI()
    }
  }

  func updateUI() {
    guard let newsVM = newsVM else { return }
    titleL.text = newsVM.title
    descriptionL.text = newsVM.description
    dateL.text = newsVM.date
    picIV.setImage(url: newsVM.picUrl)
  }
}
