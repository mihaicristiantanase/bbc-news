// ©2019 Mihai Cristian Tanase. All rights reserved.

import SideMenu
import UIKit

class WithMenuVC: UIViewController {
  override func viewDidLoad() {
    super.viewDidLoad()
    setupSideMenu()
  }

  func setupSideMenu() {
    SideMenuManager.default.menuFadeStatusBar = false
    SideMenuManager.default.menuAddPanGestureToPresent(
      toView: navigationController!.navigationBar
    )
    SideMenuManager.default.menuAddScreenEdgePanGesturesToPresent(
      toView: navigationController!.view
    )
  }
}

extension WithMenuVC: UISideMenuNavigationControllerDelegate {
  func sideMenuWillAppear(
    menu _: UISideMenuNavigationController, animated: Bool
  ) {
    print("SideMenu Appearing! (animated: \(animated))")
  }

  func sideMenuDidAppear(
    menu _: UISideMenuNavigationController, animated: Bool
  ) {
    print("SideMenu Appeared! (animated: \(animated))")
  }

  func sideMenuWillDisappear(
    menu _: UISideMenuNavigationController, animated: Bool
  ) {
    print("SideMenu Disappearing! (animated: \(animated))")
  }

  func sideMenuDidDisappear(
    menu _: UISideMenuNavigationController, animated: Bool
  ) {
    print("SideMenu Disappeared! (animated: \(animated))")
  }
}
