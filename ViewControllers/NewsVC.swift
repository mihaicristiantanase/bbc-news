// ©2019 Mihai Cristian Tanase. All rights reserved.

import UIKit

class NewsVC: WithMenuVC {
  @IBOutlet var collectionV: UICollectionView!
  @IBOutlet var msgL: UILabel!
  let newsVCViewModel = NewsVCViewModel()

  override func viewDidLoad() {
    super.viewDidLoad()
    newsVCViewModel.delegate = self
    newsVCViewModel.load { [weak self] in
      self?.updateUI()
    }
  }

  func updateUI() {
    switch newsVCViewModel.currentState {
    case .loading?:
      collectionV.isHidden = true
      msgL.isHidden = false
      msgL.text = "Se încarcă ştirile..."
    case .error?:
      collectionV.isHidden = true
      msgL.isHidden = false
      msgL.text = "A apărut o eroare!"
    case .empty?:
      collectionV.isHidden = true
      msgL.isHidden = false
      msgL.text = "Nu s-a găsit nicio ştire!"
    case .done?:
      collectionV.isHidden = false
      msgL.isHidden = true
      collectionV.reloadData()
    default:
      break
    }
  }

  @IBAction func didPressRefreshB(_: Any) {
    newsVCViewModel.load { [weak self] in
      self?.updateUI()
    }
  }

  override func viewWillTransition(
    to size: CGSize,
    with coordinator: UIViewControllerTransitionCoordinator
  ) {
    super.viewWillTransition(to: size, with: coordinator)
    if let layout = collectionV.collectionViewLayout
      as? UICollectionViewFlowLayout {
      layout.invalidateLayout()
    }
  }

  class func create() -> NewsVC {
    return UIStoryboard(name: "Main", bundle: nil).instantiateViewController(
      withIdentifier: "NewsVC"
    ) as! NewsVC
  }
}

private let cellSpacing: CGFloat = 10
private let nrCellsPerLine = 2

extension NewsVC: UICollectionViewDataSource {
  public func numberOfSections(in _: UICollectionView) -> Int {
    return 1
  }

  public func collectionView(
    _: UICollectionView,
    numberOfItemsInSection _: Int
  ) -> Int {
    return newsVCViewModel.items?.count ?? 0
  }

  public func collectionView(
    _ collectionView: UICollectionView,
    cellForItemAt indexPath: IndexPath
  ) -> UICollectionViewCell {
    let cell = collectionView.dequeueReusableCell(
      withReuseIdentifier: "news_cell", for: indexPath
    ) as! NewsCell
    cell.newsVM = NewsViewModel(item: newsVCViewModel.items![indexPath.row])
    return cell
  }
}

extension NewsVC: UICollectionViewDelegate {
  func collectionView(
    _: UICollectionView,
    didSelectItemAt indexPath: IndexPath
  ) {
    newsVCViewModel.open(indexPath.row)
  }
}

extension NewsVC: UICollectionViewDelegateFlowLayout {
  func collectionView(
    _: UICollectionView,
    layout _: UICollectionViewLayout,
    sizeForItemAt indexPath: IndexPath
  ) -> CGSize {
    // OBS(mihai): 0.5 is there just to make sure "nrCellsPerLine" is
    // respected no matter the approximations done by UICollectionViewLayout
    let w = (UIScreen.main.bounds.size.width - cellSpacing) / CGFloat(nrCellsPerLine)
      - cellSpacing - 0.5
    return CGSize(width: w, height: w)
  }

  func collectionView(
    _: UICollectionView,
    layout _: UICollectionViewLayout,
    minimumLineSpacingForSectionAt _: Int
  ) -> CGFloat {
    return cellSpacing
  }

  func collectionView(
    _: UICollectionView,
    layout _: UICollectionViewLayout,
    minimumInteritemSpacingForSectionAt _: Int
  ) -> CGFloat {
    return cellSpacing
  }

  func collectionView(
    _: UICollectionView,
    layout _: UICollectionViewLayout,
    insetForSectionAt _: Int
  ) -> UIEdgeInsets {
    return UIEdgeInsets(
      top: cellSpacing,
      left: cellSpacing,
      bottom: cellSpacing,
      right: cellSpacing
    )
  }
}

extension NewsVC: NewsVCViewModelDelegate {
  func newsVCViewModelOpenUrl(_ url: URL) {
    let vc = WebKitVC.create(url)
    present(vc, animated: true, completion: nil)
  }
}
