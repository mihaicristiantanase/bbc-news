// ©2019 Mihai Cristian Tanase. All rights reserved.

import UIKit

class MenuVC: UIViewController {
  override func viewDidLoad() {
    super.viewDidLoad()
  }

  @IBAction func didPressNewsB(_: Any) {
    navigationController?.pushViewController(NewsVC.create(), animated: true)
  }

  @IBAction func didPressSettingsB(_: Any) {
    navigationController?.pushViewController(SettingsVC.create(), animated: true)
  }

  @IBAction func didPressAboutB(_: Any) {
    navigationController?.pushViewController(AboutVC.create(), animated: true)
  }
}
