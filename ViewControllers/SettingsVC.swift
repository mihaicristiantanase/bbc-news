// ©2019 Mihai Cristian Tanase. All rights reserved.

import UIKit

class SettingsVC: WithMenuVC {
  @IBOutlet var sw: UISwitch!
  @IBOutlet var currentL: UILabel!

  override func viewDidLoad() {
    super.viewDidLoad()
    sw.isOn = AppSettings.openNewsInApp
    updateUI()
  }

  @IBAction func didChangeSwitch(_ sw: UISwitch) {
    AppSettings.openNewsInApp = sw.isOn
    updateUI()
  }

  func updateUI() {
    currentL.text = "Ştirea se deschide in " + (sw.isOn ? "aplicatie" : "Safari")
  }

  class func create() -> SettingsVC {
    return UIStoryboard(name: "Main", bundle: nil).instantiateViewController(
      withIdentifier: "SettingsVC"
    ) as! SettingsVC
  }
}
