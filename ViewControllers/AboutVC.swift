// ©2019 Mihai Cristian Tanase. All rights reserved.

import UIKit

class AboutVC: WithMenuVC {
  class func create() -> AboutVC {
    return UIStoryboard(name: "Main", bundle: nil).instantiateViewController(
      withIdentifier: "AboutVC"
    ) as! AboutVC
  }
}
