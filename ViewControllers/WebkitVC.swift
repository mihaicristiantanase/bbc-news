// ©2019 Mihai Cristian Tanase. All rights reserved.

import WebKit

class WebKitVC: UIViewController {
  var url: URL!
  var wkv: WKWebView!
  @IBOutlet var navitem: UINavigationItem!
  @IBOutlet var content: UIView!

  override func viewDidLoad() {
    super.viewDidLoad()

    let webConfiguration = WKWebViewConfiguration()
    wkv = WKWebView(frame: .zero, configuration: webConfiguration)
    wkv.translatesAutoresizingMaskIntoConstraints = false
    content.addSubview(wkv)

    var constrs: [NSLayoutConstraint] = []
    constrs.append(
      NSLayoutConstraint(
        item: wkv,
        attribute: .top,
        relatedBy: .equal,
        toItem: content,
        attribute: .top,
        multiplier: 1.0,
        constant: 0
      )
    )
    constrs.append(
      NSLayoutConstraint(
        item: wkv,
        attribute: .left,
        relatedBy: .equal,
        toItem: content,
        attribute: .left,
        multiplier: 1.0,
        constant: 0
      )
    )
    constrs.append(
      NSLayoutConstraint(
        item: wkv,
        attribute: .bottom,
        relatedBy: .equal,
        toItem: content,
        attribute: .bottom,
        multiplier: 1.0,
        constant: 0
      )
    )
    constrs.append(
      NSLayoutConstraint(
        item: wkv,
        attribute: .right,
        relatedBy: .equal,
        toItem: content,
        attribute: .right,
        multiplier: 1.0,
        constant: 0
      )
    )
    view.addConstraints(constrs)

    let req = URLRequest(url: url)
    wkv.load(req)
  }

  @IBAction func didPressCloseB(_: Any) {
    dismiss(animated: true)
  }

  class func create(_ url: URL) -> WebKitVC {
    let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(
      withIdentifier: "WebKitVC"
    ) as! WebKitVC
    vc.url = url
    return vc
  }
}
