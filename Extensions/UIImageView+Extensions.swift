// ©2019 Mihai Cristian Tanase. All rights reserved.

import SDWebImage

let placeholderI = UIImage(named: "placeholder")

extension UIImageView {
  func setImage(url: URL?) {
    guard let url = url else {
      image = placeholderI
      return
    }

    sd_setImage(with: url, placeholderImage: placeholderI, options: []) {
      [weak self] img, _, cacheType, _ in
      guard let `self` = `self` else {
        return
      }
      if let downLoadedImage = img {
        if cacheType == .none || cacheType == .disk {
          self.alpha = 0
          UIView.transition(
            with: self,
            duration: 0.3,
            options: UIView.AnimationOptions.transitionCrossDissolve,
            animations: { [weak self] () -> Void in
              self?.image = downLoadedImage
              self?.alpha = 1
            }, completion: nil
          )
        }
      } else {
        self.image = placeholderI
      }
    }
  }
}
