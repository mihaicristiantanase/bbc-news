// ©2019 Mihai Cristian Tanase. All rights reserved.

import Alamofire
import UIKit

private let bbcUrl = URL(string: "http://feeds.bbci.co.uk/news/rss.xml")

private var documentsURL: URL {
  return FileManager.default.urls(
    for: .documentDirectory,
    in: .userDomainMask
  ).first!
}

private var cachedFeed: URL {
  return documentsURL.appendingPathComponent("rss.xml")
}

class BBCNewsService {
  func load(_ callback: @escaping ([NewsModel]?) -> Void) {
    load(bbcUrl, callback)
  }

  private func load(_ url: URL?, _ callback: @escaping ([NewsModel]?) -> Void) {
    guard let url = url else {
      callback(nil)
      return
    }

    Alamofire.request(url).response { [weak self] resp in
      guard let data = resp.data, !data.isEmpty else {
        // OBS(mihai): failed, so try again with the cached feed
        if url != cachedFeed {
          self?.load(cachedFeed, callback)
        } else {
          callback(nil)
        }
        return
      }

      try? data.write(to: cachedFeed)

      let parser = BBCParser(data)
      parser.execute(callback)
    }
  }
}

class BBCParser: NSObject, XMLParserDelegate {
  var data: Data?
  var items: [NewsModel] = []
  var currentElem: String = ""
  var callback: (([NewsModel]?) -> Void)?
  var insideItem: Bool = false

  // OBS(mihai): used for building the current NewsModel
  var currentItemTitle: String = ""
  var currentItemDescription: String = ""
  var currentItemDate: String = ""
  var currentItemLink: String = ""
  var currentItemPic: String = ""

  init(_ data: Data?) {
    self.data = data
  }

  func execute(_ callback: @escaping ([NewsModel]?) -> Void) {
    guard let data = data else {
      callback(nil)
      return
    }
    self.callback = callback
    let xmlparser = XMLParser(data: data)
    xmlparser.delegate = self
    xmlparser.parse()
  }

  // MARK: - XMLParserDelegate

  func parser(
    _: XMLParser,
    didStartElement elementName: String,
    namespaceURI _: String?,
    qualifiedName _: String?,
    attributes attrs: [String: String] = [:]
  ) {
    currentElem = elementName
    if !insideItem {
      insideItem = currentElem == "item"
      if insideItem {
        currentItemTitle = ""
        currentItemDescription = ""
        currentItemDate = ""
        currentItemPic = ""
      }
    } else if "media:thumbnail" == currentElem {
      currentItemPic = attrs["url"] ?? ""
    }
  }

  func parser(_: XMLParser, foundCharacters string: String) {
    if !insideItem { return }
    let foundedChar = string.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines)
    switch currentElem {
    case "title":
      currentItemTitle += foundedChar
    case "description":
      currentItemDescription += foundedChar
    case "link":
      currentItemLink += foundedChar
    case "pubDate":
      currentItemDate += foundedChar
    case "media:thumbnail":
      currentItemPic += foundedChar
    default:
      break
    }
  }

  func parser(
    _: XMLParser,
    didEndElement elementName: String,
    namespaceURI _: String?,
    qualifiedName _: String?
  ) {
    if insideItem {
      if elementName == "item" {
        insideItem = false
      }
      if !insideItem {
        let item = NewsModel(
          title: currentItemTitle,
          description: currentItemDescription,
          date: dateFromString(currentItemDate),
          link: URL(string: currentItemLink),
          pic: URL(string: currentItemPic)
        )
        items.append(item)
      }
    }
  }

  func parserDidEndDocument(_: XMLParser) {
    callback?(items)
  }

  func parser(_: XMLParser, parseErrorOccurred _: Error) {
    callback?(items)
  }
}

private var df: DateFormatter {
  let formatter = DateFormatter()
  formatter.dateFormat = "E, d MMM yyyy HH:mm:ss z"
  return formatter
}

private func dateFromString(_ str: String) -> Date? {
  return df.date(from: str)
}
